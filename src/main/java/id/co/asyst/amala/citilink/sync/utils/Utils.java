package id.co.asyst.amala.citilink.sync.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.co.asyst.commons.core.utils.DateUtils;
import id.co.asyst.commons.core.utils.GeneratorUtils;
import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Utils {
    private static Logger LOG = LoggerFactory.getLogger(Utils.class);

    public static String generateLogid(Date date, int numdigit) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

        Random rand = new Random();

        String dateFormat = "";

        if (date != null)
            dateFormat = sdf.format(date);

        String[] arr = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "i",
                "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};

        String randomkey = "";
        for (int i = 0; i < numdigit; i++) {

            int random = rand.nextInt(36);
            randomkey += arr[random];
        }

        return dateFormat + randomkey;
    }

    @SuppressWarnings("unchecked, unused")

    public void getRes(Exchange exchange) {
        String valid = "Y";
        try {
            Map<String, Object> res = (Map<String, Object>) exchange.getIn().getBody();
            LOG.info("res :: " + res);
            LOG.info("res body :: " + exchange.getIn().getBody());
            if (res.get("before") != null) {
                if (res.get("after") == null) {
                    String type = "DELETE";
                    String typebefore = "DELETE";
                    String statustype = "NOTCHANGE";
                    Map<String, Object> before = (Map<String, Object>) res.get("before");
                    Map<String, Object> source = (Map<String, Object>) res.get("source");
                    String createdby = before.get("createdby").toString();
                    String createddate = before.get("createddate").toString();
                    String updatedby = null;
                    String updateddate = null;
                    if (before.get("updatedby") != null) {
                        updatedby = before.get("updatedby").toString();
                        updateddate = before.get("updateddate").toString();
                    }
                    String table = source.get("table").toString();
                    String id = before.get("memberid").toString();
                    if (table.equals("member_address") || table.equals("member_phones")) {
                        type = "UPDATE";
                        statustype = "CHANGE";
                        exchange.setProperty("tabletype", "member");
                    }
                    exchange.setProperty("memberid", id);
                    exchange.setProperty("type", type);
                    exchange.setProperty("typebefore", typebefore);
                    exchange.setProperty("statustype", statustype);
                    exchange.setProperty("table", table);
                    exchange.setProperty("valid", valid);
                    exchange.setProperty("createdby", createdby);
                    exchange.setProperty("createddate", createddate);
                    if (updatedby != null) {
                        exchange.setProperty("updatedby", updatedby);
                        exchange.setProperty("updateddate", updateddate);
                    } else {
                        exchange.setProperty("updatedby", null);
                        exchange.setProperty("updateddate", null);
                    }
                } else {
                    String type = "UPDATE";
                    String typebefore = "UPDATE";
                    String statusbef = "";
                    String statusaf = "";
                    String statustype = "NOTCHANGE";
                    Map<String, Object> before = (Map<String, Object>) res.get("before");
                    Map<String, Object> after = (Map<String, Object>) res.get("after");
                    Map<String, Object> source = (Map<String, Object>) res.get("source");
                    String id = after.get("memberid").toString();
                    String table = source.get("table").toString();
                    String createdby = after.get("createdby").toString();
                    String createddate = after.get("createddate").toString();
                    String updatedby = after.get("updatedby").toString();
                    String updateddate = after.get("updateddate").toString();
                    String tabletype = "";
                    if (table.equals("member_account") || table.equals("member_tier")) {
                        tabletype = "mileage";
                    } else if (table.equals("member")) {
                        tabletype = "member";
                        statusbef = before.get("status").toString();
                        statusaf = after.get("status").toString();
                        if (statusbef.equals("TERMINATED") || statusbef.equals("FRAUD") || statusbef.equals("DECEASED")) {
                            exchange.setProperty("valid", "N");
                        } else {
                            if (statusaf.equals("TERMINATED") || statusaf.equals("FRAUD") || statusaf.equals("DECEASED")) {
                                type = "DELETE";
                                statustype = "CHANGE";
                            }
                        }
                    } else {
                        tabletype = "member";
                    }
                    exchange.setProperty("statustype", statustype);
                    exchange.setProperty("valid", valid);
                    exchange.setProperty("memberid", id);
                    exchange.setProperty("tabletype", tabletype);
                    exchange.setProperty("type", type);
                    exchange.setProperty("typebefore", typebefore);
                    exchange.setProperty("table", table);
                    exchange.setProperty("createdby", createdby);
                    exchange.setProperty("createddate", createddate);
                    exchange.setProperty("updatedby", updatedby);
                    exchange.setProperty("updateddate", updateddate);
                }
            } else {
                if (res.get("after") != null) {
                    String type = "CREATE";
                    String typebefore = "CREATE";
                    String statustype = "NOTCHANGE";
                    Map<String, Object> after = (Map<String, Object>) res.get("after");
                    Map<String, Object> source = (Map<String, Object>) res.get("source");
                    String table = source.get("table").toString();
                    String id = after.get("memberid").toString();
                    String createdby = after.get("createdby").toString();
                    String createddate = after.get("createddate").toString();
                    if (table.equals("member_address") || table.equals("member_phones")) {
                        type = "UPDATE";
                        statustype = "CHANGE";
                        exchange.setProperty("tabletype", "member");
                    }
                    exchange.setProperty("tabletype", "member");
                    exchange.setProperty("statustype", statustype);
                    exchange.setProperty("memberid", id);
                    exchange.setProperty("type", type);
                    exchange.setProperty("typebefore", typebefore);
                    exchange.setProperty("valid", valid);
                    exchange.setProperty("table", table);
                    exchange.setProperty("createdby", createdby);
                    exchange.setProperty("createddate", createddate);
                    exchange.setProperty("updatedby", null);
                    exchange.setProperty("updateddate", null);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.error("Request Queue not valid");
            return;
        }
    }

    @SuppressWarnings("unchecked, unused")
    public void constructBody(Exchange exchange) {
        try {
            Map<String, Object> resultMember = exchange.getProperty("resultResponseMemberProfile", Map.class);
            String logid = generateLogid(DateUtils.today(), 7);
            String type = exchange.getProperty("type").toString();
//            String tabletype = exchange.getProperty("tabletype").toString();
            String statustype = exchange.getProperty("statustype").toString();
            String typebefore = exchange.getProperty("typebefore").toString();
            LOG.info("STATUS: " + statustype);

            Map<String, Object> response = new HashMap<>();
            response.put("type", type);
            response.put("memberinfo", resultMember);
            response.put("logid", logid);
            response.put("object", exchange.getProperty("table").toString());
            if (type.equalsIgnoreCase("UPDATE")) {
                if (!statustype.equalsIgnoreCase("CHANGE")) {
                    response.put("createdby", exchange.getProperty("createdby").toString());
                    response.put("createddate", exchange.getProperty("createddate").toString());
                    response.put("updatedby", exchange.getProperty("updatedby").toString());
                    response.put("updateddate", exchange.getProperty("updateddate").toString());
                } else {
                    if (typebefore.equalsIgnoreCase("CREATE")) {
                        response.put("createdby", exchange.getProperty("createdby").toString());
                        response.put("createddate", exchange.getProperty("createddate").toString());
                        response.put("updatedby", exchange.getProperty(null));
                        response.put("updateddate", exchange.getProperty(null));
                    } else {
                        response.put("createdby", exchange.getProperty("createdby").toString());
                        response.put("createddate", exchange.getProperty("createddate").toString());
                        if (exchange.getProperty("updatedby") != null) {
                            response.put("updatedby", exchange.getProperty("updatedby").toString());
                            response.put("updateddate", exchange.getProperty("updateddate").toString());
                        } else {
                            response.put("updatedby", exchange.getProperty(null));
                            response.put("updateddate", exchange.getProperty(null));
                        }
                    }
                }
            }

            exchange.setProperty("logid", logid);
            exchange.getOut().setBody(new ObjectMapper().writeValueAsString(response));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void generateSyncQgId(Exchange exchange) {
        String syncqgid = GeneratorUtils.GenerateId("", DateUtils.today(), 8);
        exchange.setProperty("syncqgid", syncqgid);
    }

}
