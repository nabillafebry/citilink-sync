package id.co.asyst.amala.citilink.sync;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource({"partner-sync-main.xml", "beans.xml"})
public class PasApp {

    public static void main(String[] args) {
        SpringApplication.run(PasApp.class, args);
    }
}