# Alpine Linux with OpenJDK JRE
FROM openjdk:8-jre-alpine
# copy WAR into image
COPY /citilink-sync-1.2.0-SNAPSHOT.jar /citilink-sync-1.2.0-SNAPSHOT.jar
# run application with this command line[
CMD ["java", "-jar", "/citilink-sync-1.2.0-SNAPSHOT.jar"]
